include:
    # Metadata shared by many jobs
    - local: .gitlab/rules.yml
    - local: .gitlab/artifacts.yml

    # Builds.
    - local: .gitlab/git.yml
    - local: .gitlab/cargo.yml

stages:
    - prep
    - build
    - test
    - publish

.rust_mindeps:
    image: "rust:1.63"

    variables:
        CARGO_UPDATE_POLICY: mindeps
        GIT_CLONE_PATH: $CI_BUILDS_DIR/rust

.rust_stable:
    image: "rust:latest"

    variables:
        CARGO_UPDATE_POLICY: newest
        GIT_CLONE_PATH: $CI_BUILDS_DIR/rust

.rust_nightly:
    extends: .rust_stable

    image: "rustlang/rust:nightly"

cache-newest:prep:
    extends:
        - .rust_stable
        - .cargo_fetch_job
        - .cargo_build_tags
        - .cargo_fetch_artifacts
        - .run_automatically

cache-mindeps:prep:
    extends:
        - .rust_nightly
        - .cargo_fetch_job
        - .cargo_build_tags
        - .cargo_fetch_artifacts
        - .run_automatically
    variables:
        GENERATE_LOCKFILE_ARGS: "-Z minimal-versions"
        CARGO_UPDATE_POLICY: mindeps

clippy:build:
    extends:
        - .rust_stable
        - .cargo_clippy_job
        - .cargo_build_tags
        - .run_automatically
    needs:
        - cache-newest:prep

.cargo_audit_ignore:
    variables:
        # Ignored advisories
        #
        # Regexes with large repetitions on empty sub-expressions take a very
        # long time to parse
        # https://rustsec.org/advisories/RUSTSEC-2022-0013
        # Not relevant; only static regexes are used here.
        CARGO_AUDIT_ARGS: --ignore RUSTSEC-2022-0013

semver-checks:prep:
    extends:
        - .rust_stable
        - .cargo_semver_checks_job
        - .cargo_build_tags
        - .run_automatically
    variables:
        CHANGELOG_PATH: CHANGELOG.md

audit-mindeps:build:
    extends:
        - .rust_mindeps
        - .cargo_audit_job
        - .cargo_build_tags
        - .run_automatically
        - .cargo_audit_ignore
    needs:
        - cache-mindeps:prep

audit-newest:build:
    extends:
        - .rust_stable
        - .cargo_audit_job
        - .cargo_build_tags
        - .run_automatically
        - .cargo_audit_ignore
    needs:
        - cache-newest:prep

stable:build:
    extends:
        - .rust_stable
        - .cargo_build_job
        - .cargo_build_tags
        - .cargo_build_artifacts
        - .run_automatically
    needs:
        - cache-newest:prep

stable:test:
    extends:
        - .rust_stable
        - .cargo_test_job
        - .cargo_build_tags
        - .cargo_test_artifacts
        - .run_automatically
    needs:
        - stable:build

stable:test-worktree:
    extends:
        - .rust_stable
        - .cargo_test_job
        - .cargo_build_tags
        - .cargo_test_artifacts
        - .run_automatically
    needs:
        - stable:build
    variables:
        WORKTREE: "true"

tarpaulin:build:
    extends:
        - .rust_stable
        - .cargo_tarpaulin_build_job
        - .cargo_build_tags
        - .cargo_build_artifacts
        - .run_automatically
    needs:
        - cache-newest:prep

tarpaulin:test:
    extends:
        - .rust_stable
        - .cargo_tarpaulin_test_job
        - .cargo_privileged_tags
        - .run_automatically
    needs:
        - tarpaulin:build

nightly:build:
    extends:
        - .rust_nightly
        - .cargo_build_job
        - .cargo_build_tags
        - .cargo_build_artifacts
        - .run_automatically
    needs:
        - cache-newest:prep

nightly:test:
    extends:
        - .rust_nightly
        - .cargo_test_job
        - .cargo_build_tags
        - .cargo_test_artifacts
        - .run_automatically
    needs:
        - nightly:build

nightly:test-worktree:
    extends:
        - .rust_nightly
        - .cargo_test_job
        - .cargo_build_tags
        - .cargo_test_artifacts
        - .run_automatically
    needs:
        - nightly:build
    variables:
        WORKTREE: "true"

mindeps:build:
    extends:
        - .rust_mindeps
        - .cargo_build_job
        - .cargo_build_tags
        - .cargo_build_artifacts
        - .run_automatically
    needs:
        - cache-mindeps:prep

git-bullseye:prep:
    extends:
        - .git_build_master
        - .rust_mindeps
        - .cargo_build_tags
        - .run_automatically
    variables:
        GIT_VERSION: "v2.31.8"

mindeps:test:
    extends:
        - .rust_mindeps
        - .cargo_test_job
        - .cargo_build_tags
        - .cargo_test_artifacts
        - .run_automatically
    needs:
        - git-bullseye:prep
        - mindeps:build

mindeps:test-worktree:
    extends:
        - .rust_mindeps
        - .cargo_test_job
        - .cargo_build_tags
        - .cargo_test_artifacts
        - .run_automatically
    needs:
        - git-bullseye:prep
        - mindeps:build
    variables:
        WORKTREE: "true"

git:prep:
    extends:
        - .git_build_master
        - .cargo_build_tags
        - .run_automatically

git-master:test:
    extends:
        - .rust_stable
        - .cargo_test_job
        - .cargo_build_tags
        - .cargo_test_artifacts
        - .run_automatically
    needs:
        - git:prep
        - stable:build

git-master:test-worktree:
    extends:
        - .rust_stable
        - .cargo_test_job
        - .cargo_build_tags
        - .cargo_test_artifacts
        - .run_automatically
    needs:
        - git:prep
        - stable:build
    variables:
        WORKTREE: "true"

publish:
    extends:
        - .rust_stable
        - .cargo_publish_job
        - .cargo_build_tags
        - .run_automatically
    needs:
        - job: audit-mindeps:build
          artifacts: false
        - job: audit-newest:build
          artifacts: false
        - job: clippy:build
          artifacts: false
        - job: git-master:test
          artifacts: false
        - job: git-master:test-worktree
          artifacts: false
        - job: mindeps:test
          artifacts: false
        - job: mindeps:test-worktree
          artifacts: false
        - job: nightly:test
          artifacts: false
        - job: nightly:test-worktree
          artifacts: false
        - job: stable:test
          artifacts: false
        - job: stable:test-worktree
          artifacts: false
        - job: tarpaulin:test
          artifacts: false
