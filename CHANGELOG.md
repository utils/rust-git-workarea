# v4.3.0

  * `WorkAreaError::CreateTempDirectoryPath` now exists as
    `::CreateTempDirectory` with the path that failed.
  * `GitWorkArea` now supports being launched from worktrees. This now requires
    Git 2.31.

# v4.2.6

  * Expose the `SubmoduleIntent` enumeration as it is the type of a member of
    `WorkAreaError::SubmoduleSetup`.
  * Bump minimum `tempfile` to 3.7.0 to avoid a transitive dependency on the
    unmaintained `instant` crate. The `mindeps` build now requires Rust 1.63.

# v4.2.5

  * More CI updates to fix CI-based publishing.

# v4.2.4

  * CI updates to fix CI-based publishing.

# v4.2.3

  * MSRV bumped to 1.60. This is due to updates in dependencies.

# v4.2.2

  * MSRV bumped to 1.53. While the code update is just for a small code
    simplification, this release is 17 months old at this point.
  * `tempfile` bumped to 3.4.0 to avoid a dependency on `remove_dir_all`.

# v4.2.1

  * The `regex` dependency has dropped its feature list to just `std` to avoid
    depending on crates for which CVEs have been found.

# v4.2.0

  * MSRV bumped to 1.45. While the code update is just for a small code
    simplification, in order to simplify CI, updating makes sense.

# v4.1.0

  * Arguments taking `AsRef<Path>` are now `Into<PathBuf>` if the
    implementation ends up owning the parameter anyays.
  * `GitContext::default_branch` can determine the "default" branch name for a
    checkout. This uses a new configuration parameter in Git and falls back to
    asking the `origin` remote or the `HEAD` symbolic ref if all else fails.
  * When resolving submodule conflicts, the submodule's default branch (as
    determined by the new `default_branch` method) is now used rather than
    assuming it is named `master`.

# v4.0.0

  * The `MergeCommand::author_date` method now takes a `DateTime<Utc>` rather
    than a reference.
  * The `GitWorkArea::checkout` method now requires a mutable reference. This
    is because checking out locks the index which means that it cannot be
    shared while performing this action.
  * The lifetime for merging has been moved to the `MergeCommand` structure.
    This prevents the command from outliving the workarea it uses for conflict
    files.
  * `error-chain` is no longer used. Instead, custom error types are used. This
    means that errors from this crate no longer allocate (except where a
    `String` needs to be stored for messaging purposes).
  * `Into<String>` is used for `CommitId` and `Identity` construction methods.

# v3.1.2

  * More submodule name/path mismatch fixes.

# v3.1.1

  * Submodules which have different names and paths are now properly handled.

# v3.1.0

  * Added support for "update merges". This is equivalent to using the `ours`
    merge strategy.
  * Checking out files now accepts an iterator over pathspecs.

# v3.0.0

  * Bump `chrono` from `0.3` to `0.4`. This is required for compatibility with
    `serde-1.0`.

# v2.0.0

  * Support checking out files into a prepared workarea.
  * Add `MergeCommand` to handle creation of the merge commit.
  * `GitContext::fetch` now supports an iterator over refnames.
  * Renamed `PreparedGitWorkArea` to `GitWorkArea`.

# v1.0.1

  * Documentation fixes.

# v1.0.0

  * Initial stable release.
